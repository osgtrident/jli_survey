(function(){
    var app = angular.module('surveyBody',[]);

    /*
    * PageController is the most important controller for this webapp.
    * Many of the controller's properties are determined by the survey token, which should appear in the URL as
    * ...com/?token=xxx-xxxx-xxx-a
    * From the token, the app can detect whether the survey is version A or B. In the event that no token is passed,
    * the survey defaults to the A version.
    *
    * The survey then stores the array of questions in this.products depending on the version and language.
    * Using this array of questions, the survey generates a skeleton to store the responses to the survey.
    * The structure of the responses can be seen in function getDefaultResponses().
    *
    * Because this controller spans the entirety of the HTML body, it also performs a number of miscellaneous tasks.
    * */
    app.controller('PageController', function() {
        this.accessComplete = false; //sets visibility of intro page
        this.surveyComplete = false; //no longer entirely necessary due to thank-you page redirect
        this.langSelected = 'English'; //language for the survey
        this.surveyVersion = getVersion(); //getVersion() parses the token from the URL to find the desired survey version, A or B
        this.products = pickVersion(this.langSelected,this.surveyVersion);
        /*
        This is the array of questions. Note that pickVersion also sets the qNum, gNum, and partNum properties of each
        question, group, and part - this is really important when it comes to identifying and indexing different questions.
         */
        this.responses = setResponses(this.products,this.surveyVersion,this.langSelected); //responses
        savedResponses = this.responses; //savedResponses exists outside of the controller and is used in the actual ajax POST command to the server
        this.activeQuestion=1; //question that the user is on. warning: it starts at 1, not 0.
        this.hasToken = hasToken(); //certain elements depend on whether or not there is a token
        this.changeVersion = function(lng,vrs){
            /*despite its name, changeVersion() is mostly used to change the language of the survey.
            It can be used to change the version as well if you want to debug.

            lng is the language to which you want to switch the survey as a string (should be either "English" or "Spanish")
            vrs is the version to which you want to change the survey as a string (should be "A" or "B")
             */
            this.langSelected = lng;
            this.surveyVersion = vrs;
            this.products=pickVersion(this.langSelected,this.surveyVersion);
            this.responses = setResponses(this.products,this.surveyVersion,this.langSelected);
            savedResponses = this.responses;
          //  alert((this.products[2].groups[0].questionText).toString);
            // var myEl = angular.element( document.querySelector( '#begin' ) );
            //alert(myEl.value);
           // alert(JSON.stringify(this.responses));
            //console.log(JSON.stringify(savedResponses));
            if ( this.langSelected == 'English'){
                document.getElementById("begin").textContent="Begin";
                document.getElementById("prevbtn").textContent="Previous";
                document.getElementById("nextbtn").textContent="Next";
                document.getElementById("submit").textContent="Submit";

            }
            else if( this.langSelected == 'Spanish')
            {
                document.getElementById("begin").textContent="Comenzar";
                document.getElementById("prevbtn").textContent="Ante";
                document.getElementById("nextbtn").textContent="Siguiente";
                document.getElementById("submit").textContent="Enviar";

            }
        };
        this.injectQuestions = function() {
            /*
            injectQuestions() is a very important method - it takes all the questionText properties from each question
            group in this.products and inserts them into the appropriate <span> in index.html using (node).appendChild().
            However, because Q4 may change its text depending on user input to Q2 or Q3 (depending on version), the text
            nodes are given an ID so they can be identified and modified using DOM methods.
             */
            for (var h=0;h<this.products.length;h++){

               // alert(JSON.stringify(this.products.length));
                for (var k=0;k<this.products[h].groups.length;k++){
                    //this.products[h].groups[k].questionText.setAttribute("id","text" + this.products[h].groups[k].groupNum);
                   // alert(JSON.stringify(this.products.groups.length));
                this.products[h].groups[k].questionText.setAttribute("id","text" + this.products[h].groups[k].groupNum);

                    document.getElementById("qText" + this.products[h].groups[k].groupNum).appendChild(this.products[h].groups[k].questionText);

                }
            }
        };
        this.setTimeStamp = function(qn) {
            /*
            The responses variable requires a field for a timestamp. This method updates the timestamp every time the
            "next" or "prev" button is pressed.
             */
            for (var k=0;k<this.products[qn].groups.length;k++){
                for (var h=0;h<this.products[qn].groups[k].parts.length;h++){
                    pn = this.products[qn].groups[k].parts[h].partNum;
                    this.responses.d[pn].t= new Date().getTime();
                }
            }
        };
        this.adjustDependence = function(){
            /*
            This function updates the text of dependent questions - specifically, question 4.
            It triggers when a question has the "dependsOn" property, which identifies the question on which the text
            should depend. "dependsOn" takes the format [qNum,groupNum,partNum] - for instance, in the A version of the
            survey, Q4 depends on Q2, so dependsOn = [2,1,1] - second question, first group, first part (note indexing starts from 1).
             */

            //alert(toString(tmp) );
            var tmp = this.products[this.activeQuestion-1].groups[0];
            if (tmp.dependsOn != null){
                var dependent = tmp.dependsOn;
                var dependentAns = this.responses.d[this.products[dependent[0]-1].groups[dependent[1]-1].parts[dependent[2]-1].partNum].v;
                var child = document.getElementById("text" + tmp.groupNum);
                var parent = document.getElementById("qText" + tmp.groupNum);
                var newChild;
                if (dependentAns==='Definitely will return' || dependentAns==='Definitivamente voy a regresar'){
                    newChild = tmp.questionsPossible[0];


                }
                else if (dependentAns==='Probably will return'|| dependentAns==='Probablemente voy a regresar'){
                    newChild = tmp.questionsPossible[1];
                }
                else if (dependentAns==='Probably will NOT return'|| dependentAns==='Probablemente no voy a regresar'){
                    newChild = tmp.questionsPossible[2];
                }
                else if (dependentAns==='Definitely will NOT return'|| dependentAns==='Definitivamente no voy a regresar'){
                    newChild = tmp.questionsPossible[3];
                }
                else {
                    newChild = tmp.questionsPossible[4];
                }
                newChild.setAttribute("id","text"+tmp.groupNum);
                parent.replaceChild(newChild, child);

            };

        };
        this.makeURL = function(str1) {
            //makeURL() converts the questionType property of each question into a path to an HTML file.
            return ''+str1+'.html';
        };
        this.setProgressBar = function(){
            //adjusts the progress level of the progress bar and scrolls the page to the top after every question
            $("#progress").width(this.activeQuestion*10 + '%');
            window.scrollTo(0,0);
        };
        this.updateToken = function(){
            /*
            If the url does not come with a token, then Q9 contains some fields which will help the page generate
            a replacement token.

            The token takes the form of <invoice number>-<store number>-<invoice date>-<version>-<language>.
            If any of these fields are not filled out, then the missing field is marked as "NA".
             */
            if (!this.hasToken && this.activeQuestion===10){
                var tk = '';
                var qn = this.products[8].groups[0].parts;
                var state = this.responses.d[qn[0].partNum].v;
                var states = qn[0].states;
                var store = this.responses.d[qn[1].partNum].v;
                var stores = qn[1].storesList;
                var storeNums = qn[1].storeNums;
                var invoice = this.responses.d[qn[2].partNum].v;
                if (invoice === ''){
                    invoice = 'NA';
                }
                tk = tk+invoice+'-NA-';
                var storeNum;

                if (store === '' || stores[states.indexOf(state)].indexOf(store)===-1){
                    this.responses.d[qn[1].partNum].v = '';
                    storeNum = 'NA';
                }
                else {
                    storeNum = storeNums[states.indexOf(state)][stores[states.indexOf(state)].indexOf(store)];
                }
                tk = tk+storeNum+'-NA-'+this.langSelected.charAt(0).toLowerCase();
                this.responses.token = tk;
            }
        };
        this.questionComplete = function(){
            /*
            Certain questions are required. These questions have their "required" field marked as true.
           If a question is marked as required, then the "Next" button will be disabled until all form values on the
           question no longer match their default value.

           The exception is question 9, where only the "state" field is required - here I added an extra property,
           "excepted", to account for this exception.
             */
            var qCurr = this.products[this.activeQuestion-1];
            if (qCurr.required===false){
                return true;
            }
            var qc = true;
            for (var h=0;h<qCurr.groups.length;h++){
                for (var k=0;k<qCurr.groups[h].parts.length;k++){
                    var tmp = qCurr.groups[h].parts[k];
                    if (tmp.excepted!=true){
                        qc = qc && (tmp.defaultValue != this.responses.d[tmp.partNum].v);
                    }

                }
            }

            return qc;
        }
    });

    /*
    ThumbnailController specifically governs the behavior of the picture-select question type.
     */
    app.controller('ThumbnailController',function() {
        /*the initial value of this.selectedPics doesn't matter - it's dynamically set in picture-select.html
        using ng-init.
        selectedPics indicates which pictures have been selected - it stores a 1 in the positions where a picture
        has been checked and a 0 for those that are unchecked.
        */
        this.selectedPics = [[0,0,0,0],[0,0,0,0]];
        this.convertPics = function(images){
            /*
            this.convertPics() takes in the imagePairs property and uses it to generate a default value for
            this.selectedPics.
            For reference, imagePairs, and therefore images, should be an array of image rows
            images[k] should be an array of pairs of images
             */
            var nums = [];
            for (var k=0;k<images.length;k++){
                var row = [];
                for (var h=0;h<images[k].length;h++){
                    row.push(0);
                }
                nums.push(row);
            }
            return nums;
        };
        this.toggleCheck = function(row,col){
            /*
            When a picture is clicked, toggles its corresponding value in selectedPics from 0 to 1 or vice versa.
            Additionally, when the "none of these" picture is selected (hard-coded to be the very last picture in the array),
            all other options are deselected, and when any other option is selected, the "none of these" option is
            deselected.
             */
            if (row===this.selectedPics.length-1 && col===this.selectedPics[this.selectedPics.length-1].length-1){
                for (var k=0;k<this.selectedPics.length;k++){
                    for (var h=0;h<this.selectedPics[k].length;h++){
                        this.selectedPics[k][h] = 0;
                    }
                }
                this.selectedPics[row][col] = 1;
            }
            else{
                this.selectedPics[row][col] = 1-(this.selectedPics[row][col]);
                this.selectedPics[this.selectedPics.length-1][this.selectedPics[this.selectedPics.length-1].length-1] = 0;
            }
        };
        this.csvOutput = function(sn){
            /*
            The required output format is a comma-separated list of the services which the customer has selected.
            sn is an array of strings containing the names of the services corresponding to each image.
             */
            var csv = '';
            var tmp = 0;
            for (var k=0;k<this.selectedPics.length;k++){
                for (var h=0;h<this.selectedPics[k].length;h++){
                    if (this.selectedPics[k][h]===1){
                        csv = csv + sn[tmp] + ',';
                    }
                    tmp++;
                }
            }
            return csv.substring(0,csv.length-1);
        };
    });

    /*
    As described in index.html, the ButtonController is responsible for incrementing/decrementing the active question
    and showing/hiding the Prev, Next, and Submit buttons.
    In each function, inputTest is the PageController.
     */
    app.controller('ButtonController', function($scope) {
        this.decrementQuestion = function(inputTest) {
            inputTest.activeQuestion = Math.max(inputTest.activeQuestion-1,1);
        };
        this.incrementQuestion = function(inputTest) {
            inputTest.activeQuestion = Math.min(inputTest.activeQuestion+1,inputTest.products.length);
        };
        this.isLastQuestion = function(inputTest) {
            return inputTest.activeQuestion === inputTest.products.length;
        };
        this.isFirstQuestion = function(inputTest) {
            return inputTest.activeQuestion === 1;
        }



      /*  $scope.$watch('toggle', function(){
            $scope.Begin = $scope.toggle ? 'Toggle!' : 'some text';*/
    });

    /*
    The PairController is used in the pair-slider-group question.
    In this question, pairs of attributes are shown as buttons, and either one or the other must be selected.
     */
    app.controller('PairController', function(){
        this.showSelection = function(part){
            /*
            this.showSelection edits the style attribute of each pair of buttons to indicate which one is selected.
            Because each pair of buttons is a different element in the parts array, the specific part is taken as
            an input to identify the part number and therefore the button ID.
            This is used in the event that the left button is selected.
             */
            var node = document.getElementById('pair'+part.partNum+'-0');
            node.setAttribute('style','min-width: 50px; white-space: normal;background-color:#862633;color:#ffffff');
            var node2 = document.getElementById('pair'+part.partNum+'-1');
            node2.setAttribute('class','btn btn-default btn-block');
            node2.setAttribute('style','min-width: 50px; white-space: normal;');
        };
        this.showSelection2 = function(part){
            /*
             The same as showSelection, except this is used in the event that the right button is selected.
             */
            var node = document.getElementById('pair'+part.partNum+'-1');
            node.setAttribute('style','min-width: 50px; white-space: normal;background-color:#862633;color:#ffffff');
            var node2 = document.getElementById('pair'+part.partNum+'-0');
            node2.setAttribute('class','btn btn-default btn-block');
            node2.setAttribute('style','min-width: 50px; white-space: normal;');
        };
        this.storeValue = function(part,page){
            /*
            The value that is stored takes the value "attr(x)-attr(y)". Each attribute has been assigned a specific
            number beforehand. This function generates this value so it can be stored in the response array.
            This is used in the event that the left button is selected.
             */
            page.responses.d[part.partNum].v = part.attrNums[0] + '-' + part.attrNums[1];
        };
        this.storeValue2 = function(part,page){
            //The same as storeValue2, but this is used in the event that the right button is selected.
            page.responses.d[part.partNum].v = part.attrNums[1] + '-' + part.attrNums[0];
        };
    });

    /*
    Outside of the controllers, there are a number of free-floating JavaScript functions.
    These are necessary, but could not be fit inside a controller because they are referenced from within the JavaScript
    file itself.
     */

    function getVersion() {
        /*
        Gets the survey version based on the ?token= in the URL and returns the result.
        Defaults to 'A' if no token exists.
         */
        var tok = parse('token','A');
        //var vrs = tok.charAt(tok.length-1).toUpperCase();
        var vrs = 'A';
        var tokenArray = tok.split('-');
        if (tokenArray.length > 1)
            vrs = tokenArray[3].toUpperCase(); //the 4th element is always the version

        if (vrs==='A' || vrs==='B'){
            return vrs;
        }
        else {return 'A';}
    }

    function hasToken() {
        /*
        Checks whether the survey has a token.
         */
        return !(parse('token','noToken')==='noToken');
    }

    function pickVersion(lng,vrs) {
        /*
        Returns the proper set of questions depending on which version and language the survey is using.
         */
        var pdct;
        vrs = vrs.toUpperCase();
        if (lng === 'English'){
            if (vrs === 'A'){
                pdct = questionsA;
            }
            else if (vrs === 'B'){
                pdct = questionsB;
            }
        }
        else if (lng === 'Spanish'){
            if (vrs === 'A'){
                pdct = questionsAS;
            }
            else if (vrs === 'B'){
                pdct = questionsBS;
            }
        }
        return pdct;
    }

    function setResponses(pdct, vrs, lang) {
        /*
        Sets partNum property on each part in the currently active set of questions.
        Returns a skeleton in which responses can be stored.
         */
        setPartNums(pdct);
        var rsp = getDefaultResponses(pdct, vrs, lang);
        return rsp;
    }

    function getDefaultResponses(questions,vrs,lang) {
        /*
        The responses must take the form of a JSON object.
        {
            token: taken from URL or generated from fields in Q9, NA-NA-NA-NA by default
            clientSecret: used by the backend
            d: an array containing an object for each part of each question.
            The object should take the following structure:
            {
                g: 'survey_response' (always)
                q: 'qa(part number)' (identifies which part the stored answer is for)
                t: timestamp
                v: stored response
            }
        }

        In order to generate this structure, this function takes in the list of questions, the survey version, and the language.
         */
        var defRes = {
            token: parse('token', 'NA-NA-NA-NA')+'-'+lang.charAt(0).toLowerCase(), //this is the actual version
            //token: getTestToken(vrs,lang), //token for testing purposes
            clientSecret: '4fdrt567fhcgbvleit678bcgfvh5h6k797',
            d: new Array(countParts(questions))
        };
        for (var k = 0; k < questions.length; k++) {
            for (var h = 0; h < questions[k].groups.length; h++) {
                for (var j = 0; j < questions[k].groups[h].parts.length; j++) {
                    defRes.d[questions[k].groups[h].parts[j].partNum] = {
                        g: 'survey_response',
                        q: 'qa' + (questions[k].groups[h].parts[j].partNum+1), //question number
                        t: new Date().getTime(), //timestamp
                        v: questions[k].groups[h].parts[j].defaultValue //default (blank) response
                    }
                }
            }
        }
        return defRes;
    }

    /*
    used for debugging.
     */
    function getTestToken(vrs,lang){
        var tok;
        if (parse('token','noToken')==='noToken'){
            tok='NA-NA-NA-'+vrs.toLowerCase()+'-'+lang.charAt(0).toLowerCase();
        }
        else {
            tok = '12942094-1433822400-1319-'+vrs.toLowerCase()+'-'+lang.charAt(0).toLowerCase();
        }
        return tok;
    }

    function countParts(questions) {
        /*
         counts the number of parts in the questions array sent as input.
         This is necessary because different versions have different numbers of parts.
         */
        var nParts = 0;
        for (var h=0;h<questions.length;h++){
            for (var k=0;k<questions[h].groups.length;k++){
                nParts = nParts + questions[h].groups[k].parts.length;
            }
        }
        return nParts;
    }


    function setPartNums(questions) {
        /*
        sets three very important properties in the currently active set of questions - qNum, groupNum, and partNum.
        Each of these begins from 0 so they can be directly used for indexing.
         */
        var pn = 0;
        var gn=0;
        for (var h=0;h<questions.length;h++){
            questions[h].qNum = h+1;
            var spn = 1;
            for (var k=0;k<questions[h].groups.length;k++){
                questions[h].groups[k].groupNum = gn;
                gn++;
                for (var j=0;j<questions[h].groups[k].parts.length;j++){
                    questions[h].groups[k].parts[j].partNum = pn;
                    questions[h].groups[k].parts[j].subPartNum = spn;
                    pn++; spn++;
                }
            }
        }
    }

    function contains(a, obj) {
        /*
        possibly obsolete. detects if array a contains object obj.
         */
        for (var i = 0; i < a.length; i++) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    }

    function parse(val, fallback) {
        /*
        checks to see if the url has a ?val= field and returns fallback if it does not.
        primarily used to detect and read token values.
         */
        var result = fallback;
            tmp = [];
        location.search
            //.replace ( "?", "" )
            // this is better, there might be a question mark inside
            .substr(1)
            .split("&")
            .forEach(function (item) {
                tmp = item.split("=");
                if (tmp[0] === val) result = decodeURIComponent(tmp[1]);
            });
        return result;
    }

    function generatePairs(lng) {
        /*
        Generates the parts field for Q6.
        The pairs in Q6 have to be randomized rigorously - each of the eight possible attributes is used in the left,
        but any attribute may appear 0-2 times on the right side.
        It returns an array of objects, where each object contains two arrays of two elements each - one containing
        the number of each attribute included, and the other including the attributes themselves.
        e.g. {attrNums: ['attr4', 'attr2'], sliderPair: ['Customer Lounge Comfort','Convenient Location and Hours']}
         */
        var attrs = [{
            id: 'attr1',
            text: 'Fast Service'
        }, {
            id: 'attr2',
            text: 'Convenient Location and Hours'
        }, {
            id: 'attr3',
            text: 'Quality of Work'
        }, {
            id: 'attr4',
            text: 'Customer Lounge Comfort'
        }, {
            id: 'attr5',
            text: 'Employee Knowledge'
        }, {
            id: 'attr6',
            text: 'Trustworthy Employees'
        }, {
            id: 'attr7',
            text: 'Offers More than Oil Changes'
        }, {
            id: 'attr8',
            text: 'Value for Money'
        }];
        if (lng==='s'){
            attrs = [{
                id: 'attr1',
                text: 'Rapidez del servicio'
            }, {
                id: 'attr2',
                text: 'Comodidad'
            }, {
                id: 'attr3',
                text: 'Calidad del trabajo'
            }, {
                id: 'attr4',
                text: 'Comodidad de la sala para clientes'
            }, {
                id: 'attr5',
                text: 'Conocimientos de los empleados'
            }, {
                id: 'attr6',
                text: 'Empleados confiables'
            }, {
                id: 'attr7',
                text: 'Ofrece más que cambio de aceite'
            }, {
                id: 'attr8',
                text: 'Buena relación calidad y precio'
            }];
        }
        /*
        A little more fun and exciting detail on how/why the randomization (not really important)

        There are 8 total attributes.
        Originally there were meant to be 16 total random pairs, with each attribute appearing twice on each side.
        This was cut down to 8 pairs due to length complaints.
        Rather than randomly select 8 pairs out of the original 16, I found it easier to just make it so that
        each attribute appeared once on the left side once - equivalently, I found the original 16 pairs and then
        removed one of each attribute from the left side.

        This means that, while attributes can appear on the left side only once, they can appear anywhere between 0-2
        times on the right side.
        The randomization essentially begins with a list of two of each attribute, and removes one element each time a
        valid pair is generated.
         */
        var pairs = new Array(attrs.length);
        var shell = [];
        for (var k=0;k<pairs.length;k++){
            pairs[k] = {
                sliderPair: new Array(2),
                attrNums: new Array(2),
                defaultValue: ''
            };
            pairs[k].sliderPair[0] = attrs[k].text;
            pairs[k].attrNums[0] = attrs[k].id;
            shell.push(attrs[k]); //shell.push(attrs[k]);
        }
        var nFinished = 0;
        do {
            var tmpIndex = Math.floor(Math.random()*shell.length);
            var pairExists = false;
            for (var i=0; i<nFinished; i++) {
                if (pairs[i].attrNums[0] == pairs[nFinished].attrNums[0] && pairs[i].attrNums[1] == shell[tmpIndex].id ||
                    pairs[i].attrNums[1] == pairs[nFinished].attrNums[0] && pairs[i].attrNums[0] == shell[tmpIndex].id) {
                    pairExists = true;
                    continue;
                }
            }
            if (pairs[nFinished].attrNums[0] != shell[tmpIndex].id && !pairExists){
                pairs[nFinished].sliderPair[1] = shell[tmpIndex].text;
                pairs[nFinished].attrNums[1] = shell[tmpIndex].id;
                //shell.splice(tmpIndex,1);
                nFinished = nFinished+1;
            }
        }while(nFinished<attrs.length);
        
        var pairs2 = [];
        for (k=0;k<attrs.length;k++){
            tmpIndex = Math.floor(Math.random()*pairs.length);
            pairs2.push(pairs[tmpIndex]);
            pairs.splice(tmpIndex,1);
        }

        return pairs2;
    }

    function ie9Slider() {
        /*
        This function performs some insane magic and identifies the browser/version that is being used.
        Using this identification, it returns the appropriate question type for Q5.

        IE9 cannot handle <input type="range"> so it gets a radio input instead. Everything else gets sliders.
         */
        var browser=(function(){
            var ua= navigator.userAgent, tem,
                M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
            if(/trident/i.test(M[1])){
                tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
                return 'IE '+(tem[1] || '');
            }
            if(M[1]=== 'Chrome'){
                tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
                if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
            }
            M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
            if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
            return M.join(' ');
        })();
        if (browser.toUpperCase() == "MSIE 9") {
            return 'label-radio-group';
        }
        else {
            return 'label-slider-group';
        }
    }

    function getQ9(){
        /*
        If the URL does not come with a ?token=, then the survey must request additional information from the user.
        Otherwise, this question merely asks for comments.
        Some of the additional information is used to generate a token variable that the backend can read without crashing.

        Notably, this question is marked as "required" if there is no ?token= in the URL, but due to the "excepted" property
        in many of the parts, the only part that is actually required for progress is the "state" field.

        More information can be seen in the PageController's questionComplete() function.
         */
        if (hasToken()){
            return {
                questionText: compileQuestionText(
                    [
                        plainText('Please provide any additional '),
                        makeColored('comments and recommendations','#862633'),
                        plainText(' that will help Jiffy Lube continue to win your business.')
                    ]),
                questionType: 'paragraph-group',
                parts: [{
                    defaultValue: ""
                }]
            };
        }
        else {
            return {
                questionText: compileQuestionText(
                    [
                        plainText('Can you please provide '),
                        makeColored('some additional information','#862633'),
                        plainText(' about your recent visit to Jiffy Lube?')
                    ]),
                questionType: 'visit-info',
                parts: [{
                    label: 'State',
                    states:["CA","IA","IL","IN","MD","NJ","NM","NV","NY","TN","TX","VA"],
                    defaultValue: ''
                }, {
                    label: 'Store',
                    storesList: [ ["AGOURA HILLS - 29162 ROADSIDE DR","ANAHEIM - 2181 W LINCOLN AVE","ANAHEIM - 2400 W BALL RD","ANAHEIM - 2505 E LINCOLN AVE","APPLE VALLEY - 18737 US HIGHWAY 18","APPLE VALLEY - 20754 BEAR VALLEY RD","AZUSA - 808 E ALOSTA AVE","BENICIA - 2026 COLUMBUS PKWY","CAMARILLO - 55 W. DAILY DR","CAMERON PARK - 3470 PALMER DR","COLTON - 1701 E WASHINGTON ST BLDG 100","COVINA - 21008 E ARROW HWY","DAVIS - 1625 RESEARCH PARK DR","EL DORADO HILLS - 4616 POST ST","ELK GROVE - 7405 SHELDON RD","FOLSOM - 709 E BIDWELL ST","FONTANA - 8893 SIERRA AVE","FRESNO - 3896 N BLACKSTONE AVE","HEMET - 330 N SANDERSON AVE","HESPERIA - 16701 MAIN ST","HESPERIA - 17177 BEAR VALLEY RD","LAKE ELSINORE - 311 SUMMERHILL DR","MANTECA - 1130 N MAIN ST","MOORPARK - 797 W LOS ANGELES AVE","OAKDALE - 146 N 6TH AVE","ONTARIO - 2560 S VINEYARD AVE","OXNARD - 101 W ESPLANADE DR","OXNARD - 611 S ROSE AVE","PERRIS - 118 E. RAMONA EXPY","POMONA - 2880 N GAREY AVE","RANCHO CORDOVA - 10796 OLSON DR","RIVERSIDE - 3693 LA SIERRA AVE","SACRAMENTO - 1640 FULTON AVE","SACRAMENTO - 2900 FLORIN RD","SACRAMENTO - 3447 MARCONI AVE","SACRAMENTO - 5464 FLORIN RD","SAN BERNARDINO - 4304 UNIVERSITY PKWY","SAN JACINTO - 635 S STATE ST","SIMI VALLEY - 1515 E LOS ANGELES AVE","SIMI VALLEY - 4426 E LOS ANGELES AVE","STOCKTON - 1648 E HAMMER LN","STOCKTON - 4715 WEST LN","SYLMAR - 15000 OLIVE VIEW DR","TEMECULA - 30690 RANCHO CALIFORNIA RD","THOUSAND OAKS - 1695 E THOUSAND OAKS BLVD","THOUSAND OAKS - 2905 E THOUSAND OAKS BLVD","VACAVILLE - 1013 E. MONTE VISTA AVE","VENTURA - 1098 E THOMPSON BLVD","VICTORVILLE - 15180 BEAR VALLEY RD","WILDOMAR - 32374 CLINTON KEITH ROAD"],["CEDAR FALLS - 5215 UNIVERSITY AVE","MARSHALLTOWN - 2105 S CENTER ST","MASON CITY - 2441 4TH ST SW","WATERLOO - 1423 E SAN MARNAN DR"],["CALUMET CITY - 1450 SIBLEY BLVD","LANSING - 17803 TORRENCE AVE"],["ANGOLA - 640 N. WAYNE STREET","AUBURN - 660 N. GRANDSTAFF DRIVE","AVON - 7825 E US HIGHWAY 36","BLOOMINGTON - 2621 E 3RD ST","BROWNSBURG - 1280 N GREEN ST","CARMEL - 10390 N. MICHIGAN RD.","CARMEL - 1270 S RANGE LINE RD","CARMEL - 14837 N MERIDIAN ST","CARMEL - 1495 KEYSTONE WAY","CARMEL - 1840 E 151ST ST","CHESTERTON - 802 AHRENS RD","COLUMBUS - 2480 N NATIONAL RD","ELKHART - 1206 N NAPPANEE ST","ELKHART - 2021 CASSOPOLIS ST","FORT WAYNE - 10324 MAYSVILLE RD","FORT WAYNE - 4334 COLDWATER RD","FORT WAYNE - 5312 S BEND DR","GREENWOOD - 320 S EMERSON AVE","GREENWOOD - 532 S STATE ROAD 135","GRIFFITH - 309 W RIDGE RD","INDIANAPOLIS - 10520 E WASHINGTON ST","INDIANAPOLIS - 1415 W 86TH ST","INDIANAPOLIS - 355 N SHADELAND AVE","INDIANAPOLIS - 3998 S EAST ST","INDIANAPOLIS - 4930 S EMERSON AVE","INDIANAPOLIS - 5444 W 38TH ST","INDIANAPOLIS - 5630 GEORGETOWN RD","INDIANAPOLIS - 5859 N. GERMAN CHURCH RD.","INDIANAPOLIS - 6275 N KEYSTONE AVE","INDIANAPOLIS - 6401 N COLLEGE AVE","INDIANAPOLIS - 7072 EMBLEM DR","INDIANAPOLIS - 7220 W 10TH ST","INDIANAPOLIS - 7619 E 96TH ST","INDIANAPOLIS - 7965 US HIGHWAY 31 S","INDIANAPOLIS - 7969 PENDLETON PIKE","INDIANAPOLIS - 8175 ALLISONVILLE RD","INDIANAPOLIS - 8580 N MICHIGAN RD","INDIANAPOLIS - 9825 FALL CREEK RD","KENDALLVILLE - 317 W. NORTH STREET","LA PORTE - 512 J ST","LAFAYETTE - 3115 SOUTH ST","MERRILLVILLE - 5920 BROADWAY","MERRILLVILLE - 9301 BROADWAY","MICHIGAN CITY - 3325 FRANKLIN ST","MISHAWAKA - 120 E MCKINLEY AVE","SOUTH BEND - 4425 SOUTH MICHIGAN STREET","VALPARAISO - 355 MORTHLAND DR"],["TAKOMA PARK - 6510 NEW HAMPSHIRE AVE"],["COLONIA - 1171 ST. GEORGES AVENUE","WOODBRIDGE - 424 KING GEORGES RD"],["LAS CRUCES - 1805 E LOHMAN AVE","LAS CRUCES - 2765 N MAIN ST","LAS CRUCES - 800 EL PASEO RD","ROSWELL - 2613 N MAIN ST"],["HENDERSON - 10440 S EASTERN AVE","HENDERSON - 130 S STEPHANIE ST","HENDERSON - 2583 WINDMILL PKWY","HENDERSON - 515 MARKS ST","LAS VEGAS - 10157 W CHARLESTON BLVD # 420","LAS VEGAS - 1111 S FORT APACHE RD","LAS VEGAS - 1409 N EASTERN AVE","LAS VEGAS - 2020 E SAHARA AVE","LAS VEGAS - 333 S DECATUR BLVD","LAS VEGAS - 3420 S RAINBOW BLVD","LAS VEGAS - 4310 E CHARLESTON BLVD","LAS VEGAS - 4320 S DURANGO DR","LAS VEGAS - 4511 E TROPICANA AVE","LAS VEGAS - 4531 N RANCHO RD","LAS VEGAS - 7215 S DURANGO DR","LAS VEGAS - 9520 W TROPICANA AVE","N LAS VEGAS - 5475 CAMINO AL NORTE","NORTH LAS VEGAS - 4716 W CRAIG RD"],["ALBANY - 1755 CENTRAL AVE","CLIFTON PARK - 1672 ROUTE 9","DELMAR - 55 DELAWARE AVE","GUILDERLAND - 2067 WESTERN AVE","HUDSON - 318 FAIRVIEW AVE","KINGSTON - 1091 ULSTER AVE","LATHAM - 711 TROY SCHENECTADY RD","QUEENSBURY - 265 QUAKER RD","RENSSELAER - 334 COLUMBIA TPKE","WILTON - 5 LOWES DRIVE"],["CLINTON - 750 CHARLES G SEIVERS BLVD","KNOXVILLE - 1002 N CEDAR BLUFF RD","KNOXVILLE - 11012 KINGSTON PIKE","KNOXVILLE - 4733 N BROADWAY ST","KNOXVILLE - 6508 CHAPMAN HWY","KNOXVILLE - 6700 CLINTON HWY","KNOXVILLE - 8031 KINGSTON PIKE","MARYVILLE - 305 WHITECREST DR"],["ARLINGTON - 1531 S. COOPER STREET","ARLINGTON - 4810 MATLOCK ROAD","ARLINGTON - 5710 FOREST BEND DR","ARLINGTON - 5855 S. COOPER STREET","AZLE - 11415 FM 730 N","BAYTOWN - 3414 GARTH RD","BOERNE - 1345 S MAIN ST","CONROE - 1112 N LOOP 336 W","CONROE - 1301 N FRAZIER ST","CYPRESS - 13102 LOUETTA RD","DEER PARK - 3417 CENTER ST","DUNCANVILLE - 1011 S. CEDAR RIDGE DR.","EULESS - 1012 N INDUSTRIAL BLVD","FORNEY - 505 N. FM 548","FORT WORTH - 3172 BASSWOOD BLVD","FORT WORTH - 6253 MCCART AVE","FORT WORTH - 7601 HIGHWAY 80 W","FRIENDSWOOD - 224 E PARKWOOD AVE","GRANBURY - 906 E. HIGHWAY 377","GRAND PRAIRIE - 1020 W PIONEER PKWY","GRAND PRAIRIE - 3030 W CAMP WISDOM RD","HOUSTON - 10605 FUQUA ST","HOUSTON - 14450 HILLCROFT AVE","HOUSTON - 14531 MEMORIAL DR","HOUSTON - 1619 GESSNER DR","HOUSTON - 2350 S DAIRY ASHFORD ST","HOUSTON - 3302 ELLA BLVD","HOUSTON - 3435 W HOLCOMBE BLVD","HOUSTON - 4513 HIGHWAY 6 N","HOUSTON - 5248 FM 1960 RD W","HOUSTON - 535 UVALDE RD","HOUSTON - 6330 WEST RD","HOUSTON - 6445 GULFTON ST","HOUSTON - 7546 BELLFORT ST","HOUSTON - 7550 HIGHWAY 6 N","HOUSTON - 9666 WESTHEIMER RD","HUMBLE - 1900 FM 1960 BYPASS RD E","HUNTSVILLE - 1501 11TH ST","HURST - 1841 PRECINCT LINE ROAD","HURST - 800 W BEDFORD EULESS RD","KATY - 21910 PROVINCIAL BLVD","KINGWOOD - 1890 NORTHPARK DR","KINGWOOD - 3227 W LAKE HOUSTON PKWY","LAKE JACKSON - 114 HIGHWAY 332","LEAGUE CITY - 2206 E MAIN ST","MESQUITE - 1019 NEVILLE CT","MESQUITE - 110 S GALLOWAY AVE","MISSOURI CITY - 6171 HIGHWAY 6","NEW BRAUNFELS - 1466 S SEGUIN AVE","PANTEGO - 2105 W PIONEER PKWY","PASADENA - 3939 BURKE RD","PASADENA - 6919 SPENCER HWY","PEARLAND - 2118 N MAIN ST","PEARLAND - 3021 BROADWAY ST","PEARLAND - 8525 BROADWAY ST","PLANO - 4144 W SPRING CREEK PARKWAY","SAN ANTONIO - 13803 NACOGDOCHES RD","SAN ANTONIO - 1564 AUSTIN HWY","SAN ANTONIO - 15703B SAN PEDRO AVE","SAN ANTONIO - 3015 THOUSAND OAKS DR","SAN ANTONIO - 4119 FREDERICKSBURG RD","SAN ANTONIO - 5584 WALZEM RD","SAN ANTONIO - 5612 BANDERA RD","SAN ANTONIO - 5921 SAN PEDRO AVE","SAN ANTONIO - 6603 FM HIGHWAY 78","SAN ANTONIO - 7083 BANDERA RD","SAN ANTONIO - 7862 CALLAGHAN RD","SAN ANTONIO - 7903 FREDERICKSBURG RD","SAN ANTONIO - 8366 MARBACH RD","SAN ANTONIO - 9150 GRISSOM RD","SAN ANTONIO - 9206 PERRIN BEITEL RD","SPRING - 185 CYPRESSWOOD DR","SPRING - 4835 LOUETTA RD","SPRING - 6518 FM 2920 RD","SPRING - 8275 LOUETTA RD","SUGARLAND - 20007 US HWY 59","TEXAS CITY - 3314 PALMER HWY","THE WOODLANDS - 511 SAWDUST RD","THE WOODLANDS - 6770 WOODLANDS PKWY","TOMBALL - 28056 STATE HIGHWAY 249","UNIVERSAL CITY - 2217 PAT BOOKER RD","WAXAHACHIE - 902 FERRIS AVENUE","WEATHERFORD - 1807 S MAIN ST","WEBSTER - 529 EL DORADO BLVD"],["CHARLOTTESVILLE - 1245 SEMINOLE TRL","CHARLOTTESVILLE - 2088 BERKMAR DR","CHARLOTTESVILLE - 240 PANTOPS CIRCLE","DANVILLE - 601 PINEY FOREST RD","FOREST - 17629 FOREST RD","LYNCHBURG - 20423 TIMBERLAKE RD","LYNCHBURG - 3209 OLD FOREST RD","MADISON HEIGHTS - 4864 S AMHERST HWY","ROANOKE - 1477 PETERS CREEK RD NW","ROANOKE - 3559 FRANKLIN RD SW","STAUNTON - 911 GREENVILLE AVE"]    ],
                    storeNums: [[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50],[51,52,53,54],[55,56],[57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103],[104],[105,106],[107,108,109,110],[111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128],[129,130,131,132,133,134,135,136,137,138],[139,140,141,142,143,144,145,146],[147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230],[231,232,233,234,235,236,237,238,239,240,241,242,243,244,245]],
                    defaultValue: '',
                    excepted: true
                }, {
                    label: 'Invoice Number (appears on receipt)',
                    defaultValue: "",
                    excepted: true
                }, {
                    label: 'Date of Visit',
                    defaultValue: "",
                    excepted: true
                }, {
                    label: 'Approximate Time of Visit',
                    defaultValue: "",
                    excepted: true
                }, {
                    label: 'Comments/Concerns',
                    defaultValue: "",
                    excepted: true
                }]
            };
        }
    }

    function getQ9Spanish(){
        /*
        same as qetq9 for spanish language.***
       */
        if (hasToken()){
            return {
                questionText: compileQuestionText(
                    [
                        plainText('Por favor, proporcione cualquier adicional '),
                        makeColored('comentarios y recomendaciones','#862633'),
                        plainText(' que ayudará a Jiffy Lube continuar para ganar su negocio.')
                    ]),
                questionType: 'paragraph-group',
                parts: [{
                    defaultValue: ""
                }]
            };
        }
        else {
            return {
                questionText: compileQuestionText(
                    [
                        plainText('¿Puede, por favor, proporcionar '),
                        makeColored('más información','#862633'),
                        plainText(' acerca de su última visita a Jiffy Lube?')
                    ]),
                questionType: 'visit-info',
                parts: [{
                    label: 'Estado',
                    states: ["CA","IA","IL","IN","MD","NJ","NM","NV","NY","TN","TX","VA"],
                    defaultValue: ''
                }, {
                    label: 'Almacén',
                    storesList: [ ["AGOURA HILLS - 29162 ROADSIDE DR","ANAHEIM - 2181 W LINCOLN AVE","ANAHEIM - 2400 W BALL RD","ANAHEIM - 2505 E LINCOLN AVE","APPLE VALLEY - 18737 US HIGHWAY 18","APPLE VALLEY - 20754 BEAR VALLEY RD","AZUSA - 808 E ALOSTA AVE","BENICIA - 2026 COLUMBUS PKWY","CAMARILLO - 55 W. DAILY DR","CAMERON PARK - 3470 PALMER DR","COLTON - 1701 E WASHINGTON ST BLDG 100","COVINA - 21008 E ARROW HWY","DAVIS - 1625 RESEARCH PARK DR","EL DORADO HILLS - 4616 POST ST","ELK GROVE - 7405 SHELDON RD","FOLSOM - 709 E BIDWELL ST","FONTANA - 8893 SIERRA AVE","FRESNO - 3896 N BLACKSTONE AVE","HEMET - 330 N SANDERSON AVE","HESPERIA - 16701 MAIN ST","HESPERIA - 17177 BEAR VALLEY RD","LAKE ELSINORE - 311 SUMMERHILL DR","MANTECA - 1130 N MAIN ST","MOORPARK - 797 W LOS ANGELES AVE","OAKDALE - 146 N 6TH AVE","ONTARIO - 2560 S VINEYARD AVE","OXNARD - 101 W ESPLANADE DR","OXNARD - 611 S ROSE AVE","PERRIS - 118 E. RAMONA EXPY","POMONA - 2880 N GAREY AVE","RANCHO CORDOVA - 10796 OLSON DR","RIVERSIDE - 3693 LA SIERRA AVE","SACRAMENTO - 1640 FULTON AVE","SACRAMENTO - 2900 FLORIN RD","SACRAMENTO - 3447 MARCONI AVE","SACRAMENTO - 5464 FLORIN RD","SAN BERNARDINO - 4304 UNIVERSITY PKWY","SAN JACINTO - 635 S STATE ST","SIMI VALLEY - 1515 E LOS ANGELES AVE","SIMI VALLEY - 4426 E LOS ANGELES AVE","STOCKTON - 1648 E HAMMER LN","STOCKTON - 4715 WEST LN","SYLMAR - 15000 OLIVE VIEW DR","TEMECULA - 30690 RANCHO CALIFORNIA RD","THOUSAND OAKS - 1695 E THOUSAND OAKS BLVD","THOUSAND OAKS - 2905 E THOUSAND OAKS BLVD","VACAVILLE - 1013 E. MONTE VISTA AVE","VENTURA - 1098 E THOMPSON BLVD","VICTORVILLE - 15180 BEAR VALLEY RD","WILDOMAR - 32374 CLINTON KEITH ROAD"],["CEDAR FALLS - 5215 UNIVERSITY AVE","MARSHALLTOWN - 2105 S CENTER ST","MASON CITY - 2441 4TH ST SW","WATERLOO - 1423 E SAN MARNAN DR"],["CALUMET CITY - 1450 SIBLEY BLVD","LANSING - 17803 TORRENCE AVE"],["ANGOLA - 640 N. WAYNE STREET","AUBURN - 660 N. GRANDSTAFF DRIVE","AVON - 7825 E US HIGHWAY 36","BLOOMINGTON - 2621 E 3RD ST","BROWNSBURG - 1280 N GREEN ST","CARMEL - 10390 N. MICHIGAN RD.","CARMEL - 1270 S RANGE LINE RD","CARMEL - 14837 N MERIDIAN ST","CARMEL - 1495 KEYSTONE WAY","CARMEL - 1840 E 151ST ST","CHESTERTON - 802 AHRENS RD","COLUMBUS - 2480 N NATIONAL RD","ELKHART - 1206 N NAPPANEE ST","ELKHART - 2021 CASSOPOLIS ST","FORT WAYNE - 10324 MAYSVILLE RD","FORT WAYNE - 4334 COLDWATER RD","FORT WAYNE - 5312 S BEND DR","GREENWOOD - 320 S EMERSON AVE","GREENWOOD - 532 S STATE ROAD 135","GRIFFITH - 309 W RIDGE RD","INDIANAPOLIS - 10520 E WASHINGTON ST","INDIANAPOLIS - 1415 W 86TH ST","INDIANAPOLIS - 355 N SHADELAND AVE","INDIANAPOLIS - 3998 S EAST ST","INDIANAPOLIS - 4930 S EMERSON AVE","INDIANAPOLIS - 5444 W 38TH ST","INDIANAPOLIS - 5630 GEORGETOWN RD","INDIANAPOLIS - 5859 N. GERMAN CHURCH RD.","INDIANAPOLIS - 6275 N KEYSTONE AVE","INDIANAPOLIS - 6401 N COLLEGE AVE","INDIANAPOLIS - 7072 EMBLEM DR","INDIANAPOLIS - 7220 W 10TH ST","INDIANAPOLIS - 7619 E 96TH ST","INDIANAPOLIS - 7965 US HIGHWAY 31 S","INDIANAPOLIS - 7969 PENDLETON PIKE","INDIANAPOLIS - 8175 ALLISONVILLE RD","INDIANAPOLIS - 8580 N MICHIGAN RD","INDIANAPOLIS - 9825 FALL CREEK RD","KENDALLVILLE - 317 W. NORTH STREET","LA PORTE - 512 J ST","LAFAYETTE - 3115 SOUTH ST","MERRILLVILLE - 5920 BROADWAY","MERRILLVILLE - 9301 BROADWAY","MICHIGAN CITY - 3325 FRANKLIN ST","MISHAWAKA - 120 E MCKINLEY AVE","SOUTH BEND - 4425 SOUTH MICHIGAN STREET","VALPARAISO - 355 MORTHLAND DR"],["TAKOMA PARK - 6510 NEW HAMPSHIRE AVE"],["COLONIA - 1171 ST. GEORGES AVENUE","WOODBRIDGE - 424 KING GEORGES RD"],["LAS CRUCES - 1805 E LOHMAN AVE","LAS CRUCES - 2765 N MAIN ST","LAS CRUCES - 800 EL PASEO RD","ROSWELL - 2613 N MAIN ST"],["HENDERSON - 10440 S EASTERN AVE","HENDERSON - 130 S STEPHANIE ST","HENDERSON - 2583 WINDMILL PKWY","HENDERSON - 515 MARKS ST","LAS VEGAS - 10157 W CHARLESTON BLVD # 420","LAS VEGAS - 1111 S FORT APACHE RD","LAS VEGAS - 1409 N EASTERN AVE","LAS VEGAS - 2020 E SAHARA AVE","LAS VEGAS - 333 S DECATUR BLVD","LAS VEGAS - 3420 S RAINBOW BLVD","LAS VEGAS - 4310 E CHARLESTON BLVD","LAS VEGAS - 4320 S DURANGO DR","LAS VEGAS - 4511 E TROPICANA AVE","LAS VEGAS - 4531 N RANCHO RD","LAS VEGAS - 7215 S DURANGO DR","LAS VEGAS - 9520 W TROPICANA AVE","N LAS VEGAS - 5475 CAMINO AL NORTE","NORTH LAS VEGAS - 4716 W CRAIG RD"],["ALBANY - 1755 CENTRAL AVE","CLIFTON PARK - 1672 ROUTE 9","DELMAR - 55 DELAWARE AVE","GUILDERLAND - 2067 WESTERN AVE","HUDSON - 318 FAIRVIEW AVE","KINGSTON - 1091 ULSTER AVE","LATHAM - 711 TROY SCHENECTADY RD","QUEENSBURY - 265 QUAKER RD","RENSSELAER - 334 COLUMBIA TPKE","WILTON - 5 LOWES DRIVE"],["CLINTON - 750 CHARLES G SEIVERS BLVD","KNOXVILLE - 1002 N CEDAR BLUFF RD","KNOXVILLE - 11012 KINGSTON PIKE","KNOXVILLE - 4733 N BROADWAY ST","KNOXVILLE - 6508 CHAPMAN HWY","KNOXVILLE - 6700 CLINTON HWY","KNOXVILLE - 8031 KINGSTON PIKE","MARYVILLE - 305 WHITECREST DR"],["ARLINGTON - 1531 S. COOPER STREET","ARLINGTON - 4810 MATLOCK ROAD","ARLINGTON - 5710 FOREST BEND DR","ARLINGTON - 5855 S. COOPER STREET","AZLE - 11415 FM 730 N","BAYTOWN - 3414 GARTH RD","BOERNE - 1345 S MAIN ST","CONROE - 1112 N LOOP 336 W","CONROE - 1301 N FRAZIER ST","CYPRESS - 13102 LOUETTA RD","DEER PARK - 3417 CENTER ST","DUNCANVILLE - 1011 S. CEDAR RIDGE DR.","EULESS - 1012 N INDUSTRIAL BLVD","FORNEY - 505 N. FM 548","FORT WORTH - 3172 BASSWOOD BLVD","FORT WORTH - 6253 MCCART AVE","FORT WORTH - 7601 HIGHWAY 80 W","FRIENDSWOOD - 224 E PARKWOOD AVE","GRANBURY - 906 E. HIGHWAY 377","GRAND PRAIRIE - 1020 W PIONEER PKWY","GRAND PRAIRIE - 3030 W CAMP WISDOM RD","HOUSTON - 10605 FUQUA ST","HOUSTON - 14450 HILLCROFT AVE","HOUSTON - 14531 MEMORIAL DR","HOUSTON - 1619 GESSNER DR","HOUSTON - 2350 S DAIRY ASHFORD ST","HOUSTON - 3302 ELLA BLVD","HOUSTON - 3435 W HOLCOMBE BLVD","HOUSTON - 4513 HIGHWAY 6 N","HOUSTON - 5248 FM 1960 RD W","HOUSTON - 535 UVALDE RD","HOUSTON - 6330 WEST RD","HOUSTON - 6445 GULFTON ST","HOUSTON - 7546 BELLFORT ST","HOUSTON - 7550 HIGHWAY 6 N","HOUSTON - 9666 WESTHEIMER RD","HUMBLE - 1900 FM 1960 BYPASS RD E","HUNTSVILLE - 1501 11TH ST","HURST - 1841 PRECINCT LINE ROAD","HURST - 800 W BEDFORD EULESS RD","KATY - 21910 PROVINCIAL BLVD","KINGWOOD - 1890 NORTHPARK DR","KINGWOOD - 3227 W LAKE HOUSTON PKWY","LAKE JACKSON - 114 HIGHWAY 332","LEAGUE CITY - 2206 E MAIN ST","MESQUITE - 1019 NEVILLE CT","MESQUITE - 110 S GALLOWAY AVE","MISSOURI CITY - 6171 HIGHWAY 6","NEW BRAUNFELS - 1466 S SEGUIN AVE","PANTEGO - 2105 W PIONEER PKWY","PASADENA - 3939 BURKE RD","PASADENA - 6919 SPENCER HWY","PEARLAND - 2118 N MAIN ST","PEARLAND - 3021 BROADWAY ST","PEARLAND - 8525 BROADWAY ST","PLANO - 4144 W SPRING CREEK PARKWAY","SAN ANTONIO - 13803 NACOGDOCHES RD","SAN ANTONIO - 1564 AUSTIN HWY","SAN ANTONIO - 15703B SAN PEDRO AVE","SAN ANTONIO - 3015 THOUSAND OAKS DR","SAN ANTONIO - 4119 FREDERICKSBURG RD","SAN ANTONIO - 5584 WALZEM RD","SAN ANTONIO - 5612 BANDERA RD","SAN ANTONIO - 5921 SAN PEDRO AVE","SAN ANTONIO - 6603 FM HIGHWAY 78","SAN ANTONIO - 7083 BANDERA RD","SAN ANTONIO - 7862 CALLAGHAN RD","SAN ANTONIO - 7903 FREDERICKSBURG RD","SAN ANTONIO - 8366 MARBACH RD","SAN ANTONIO - 9150 GRISSOM RD","SAN ANTONIO - 9206 PERRIN BEITEL RD","SPRING - 185 CYPRESSWOOD DR","SPRING - 4835 LOUETTA RD","SPRING - 6518 FM 2920 RD","SPRING - 8275 LOUETTA RD","SUGARLAND - 20007 US HWY 59","TEXAS CITY - 3314 PALMER HWY","THE WOODLANDS - 511 SAWDUST RD","THE WOODLANDS - 6770 WOODLANDS PKWY","TOMBALL - 28056 STATE HIGHWAY 249","UNIVERSAL CITY - 2217 PAT BOOKER RD","WAXAHACHIE - 902 FERRIS AVENUE","WEATHERFORD - 1807 S MAIN ST","WEBSTER - 529 EL DORADO BLVD"],["CHARLOTTESVILLE - 1245 SEMINOLE TRL","CHARLOTTESVILLE - 2088 BERKMAR DR","CHARLOTTESVILLE - 240 PANTOPS CIRCLE","DANVILLE - 601 PINEY FOREST RD","FOREST - 17629 FOREST RD","LYNCHBURG - 20423 TIMBERLAKE RD","LYNCHBURG - 3209 OLD FOREST RD","MADISON HEIGHTS - 4864 S AMHERST HWY","ROANOKE - 1477 PETERS CREEK RD NW","ROANOKE - 3559 FRANKLIN RD SW","STAUNTON - 911 GREENVILLE AVE"]    ],
                    storeNums: [[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50],[51,52,53,54],[55,56],[57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103],[104],[105,106],[107,108,109,110],[111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128],[129,130,131,132,133,134,135,136,137,138],[139,140,141,142,143,144,145,146],[147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230],[231,232,233,234,235,236,237,238,239,240,241,242,243,244,245]],
                    defaultValue: '',
                    excepted: true
                }, {
                    label: 'Número de factura (aparece en el recibo)',
                    defaultValue: "",
                    excepted: true
                }, {
                    label: 'Fecha de la visita',
                    defaultValue: "",
                    excepted: true
                }, {
                    label: 'Tiempo aproximado de la visita ',
                    defaultValue: "",
                    excepted: true
                }, {
                    label: 'Comentarios/inquietudes',
                    defaultValue: "",
                    excepted: true
                }]
            };
        }
    }


    function compileQuestionText(assemblyLine){
        /*
        This function returns a formatted HTML node which contains all the nodes in assemblyLine as children.
        input assemblyLine is an array of HTML nodes.

        For the purposes of this document, compileQuestionText is always run with outputs from plainText() or makeColored().
         */
        var para = document.createElement("b");
        for (var k=0;k<assemblyLine.length;k++){
            para.appendChild(assemblyLine[k]);
        }
        para.style.fontSize = 'large';
        //var para2 = document.createElement("p");
        //para2.appendChild(para);
        return para;
    }

    function compileSubpartText(assemblyLine){
        /*
        Appears to be unused. Was at one point used to generate text for questions with multiple groups.
         */
        var para = document.createElement("b");
        for (var k=0;k<assemblyLine.length;k++){
            para.appendChild(assemblyLine[k]);
        }
        return para;
    }

    function plainText(text){
        /*
        Converts a string into an HTML text node with no additional formatting. The output is usually sent to
        compileQuestionText() in an array.
         */
        return document.createTextNode(text);
    }

    function makeColored(text,txtColor){
        /*
        Converts a string into an HTML text node with color added. The output is usually sent to compileQuestionText()
        in an array.

        txtColor is a string containing a hexadecimal color code in the format '#xxxxxx'
         */
        var para = document.createElement("span");
        para.appendChild(document.createTextNode(text));
        para.style.color = txtColor;
        return para;
    }

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    /*
    questionsA, questionsB, questionsAS, and questionsBS are the four variables which stores the questions for the
    four different possible surveys that can be generated depending on language and survey version

    They each follow the same format -
    an array of objects with the properties
        required: specifies whether a question is required
        groups: an array of objects with the properties
            questionText: an HTML node which contains formatted text, usually generated via compileQuestionText(),
                plainText(), and makeColored()
            questionType: specifies the type of question - should be the name of an HTML file, minus the .html
            parts: an array of objects with the properties
                defaultValue: a value to send in in case the user does not answer the question
                and any other specific values that the specified question type might require.

     Also, although it is not coded here, each question, group, and part have a qNum, groupNum, and partNum attached to
     it upon execution.
     */
    var questionsA = [{
        //q1
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Please rate your overall '),
                    makeColored('satisfaction with the service','#862633'),
                    plainText(' you received during your last visit to Jiffy Lube.')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Excellent','Very Good','Average','Fair','Poor'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q2
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('How likely are you to return to Jiffy Lube the next time your vehicle needs an '),
                    makeColored('oil change','#862633'),
                    plainText('?')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Definitely will return','Probably will return','Undecided about returning','Probably will NOT return','Definitely will NOT return'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q3
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('How likely are you to return to Jiffy Lube the next time your vehicle needs '),
                    makeColored('other routine maintenance service','#862633'),
                    plainText('?')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Definitely will return','Probably will return','Undecided about returning','Probably will NOT return','Definitely will NOT return'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q4
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('probably will not return','#862633'),
                    plainText(' to Jiffy Lube the next time you need an oil change?')
                ]),
            questionsPossible: [compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('definitely will return','#862633'),
                    plainText(' to Jiffy Lube the next time you need an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('probably will return','#862633'),
                    plainText(' to Jiffy Lube the next time you need an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('probably will not return','#862633'),
                    plainText(' to Jiffy Lube the next time you need an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('definitely will not return','#862633'),
                    plainText(' to Jiffy Lube the next time you need an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you are '),
                    makeColored('undecided about returning','#862633'),
                    plainText(' to Jiffy Lube the next time you need an oil change?')
                ])],
            questionType: 'paragraph-group',
            dependsOn: [2,1,1], //question 2, first group, first part
            parts: [{
                defaultValue: ""
            }]
        }],
        required:false
    }, {
        //q5
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Based on your most recent visit at Jiffy Lube, please indicate how well the following statements '),
                    makeColored('describe your experience','#862633'),
                    plainText('.')
                ]),
            questionType: ie9Slider(),
            parts: [{
                label: 'Employees were knowledgeable about my vehicle',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'Employees were friendly and respectful',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'I trusted the service advisor\'s recommendations',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'The service was timely',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'Service was a good value for my money',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'Jiffy Lube employees are well trained to provide services other than oil changes',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }]
        }],
        required:false
    }, {
        //q6
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText(' Many different factors impact where you decide to take your vehicle for service. ' +
                        'For each pair, select the factor that was MOST important when you decided to visit Jiffy Lube for your last service.')
                ]),
            questionType: 'pair-slider-group',
            parts: generatePairs('e')
        }],
        required: false
    }, {
        //q7
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText(' To the best of your knowledge, which of the following services are offered ' +
                        'in addition to oil changes at the Jiffy Lube you visited for your last maintenance service?')
                ]),
            questionType:'picture-select',
            parts: [{
                imagePairs: [
                    [["images/battery_service.png","images/battery_service_checked.png"],
                        ["images/cooling_service.png","images/cooling_service_checked.png"],
                        ["images/fuel_service.png","images/fuel_service_checked.png"],
                        ["images/tire_rotation.png","images/tire_rotation_checked.png"]],
                    [["images/transmission_service.png","images/transmission_service_checked.png"],
                        ["images/windshield_service.png","images/windshield_service_checked.png"],
                        ["images/none.png","images/none_checked.png"]]
                ],
                serviceNames: [
                    'battery maintenance/replacement',
                    'cooling system services',
                    'fuel system cleaning services',
                    'tire rotation',
                    'transmission services',
                    'wiper blade replacement',
                    'none of these'
                ],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q8
        groups: [{
            questionText:compileQuestionText(
                [
                    plainText('Would you tell us '),
                    makeColored('about yourself','#862633'),
                    plainText('?')
                ]),
            questionType: 'personal-information-group',
            required: true,
            parts: [{
                label: 'What is your gender?',
                radioOptions: [
                    'Male',
                    'Female',
                    'Other/Prefer Not to Answer'
                ],
                defaultValue: ''
            }, {
                label: 'What is your age group?',
                label2: 'What is your age?',
                //radioOptions: [
                //    '16-35',
                //    '36-55',
                //    '56-75',
                //    '76+',
                //    'Prefer not to answer'
                //],
                radioOptions: [
                    '16-25',
                    '26-35',
                    '36-45',
                    '46-55',
                    '56-65',
                    '66+',
                    'Prefer not to answer'
                ],
                defaultValue: ''
            }]
        }],
        required: false
    }, {
        //q9
        groups: [getQ9()],
        required: !hasToken()
    }, {
        //q10
        groups: [{
            questionText:compileQuestionText(
                [
                    makeColored('(OPTIONAL)','#862633'),
                    plainText(' To discuss any compliments or concerns, ' +
                        'please provide your preferred method of contact (phone or email), and the best time to discuss.')
                ]),
            questionType: 'contact-group',
            parts: [{
                label: 'Name',
                defaultValue: ''
            }, {
                label: 'E-mail',
                defaultValue: ''
            }, {
                label: 'Phone Number',
                defaultValue: ''
            }, {
                label: 'Best days to contact',
                dropOptions: [
                    'Weekdays',
                    'Weekend',
                    'Any'
                ],
                defaultValue: ''
            }, {
                label: 'Best time to contact',
                dropOptions: [
                    '8:00 AM - 9:00 AM',
                    '9:00 AM - 10:00 AM',
                    '10:00 AM - 11:00 AM',
                    '11:00 AM - 12:00 PM',
                    '12:00 PM - 1:00 PM',
                    '1:00 PM - 2:00 PM',
                    '2:00 PM - 3:00 PM',
                    '3:00 PM - 4:00 PM',
                    '4:00 PM - 5:00 PM',
                    '5:00 PM - 6:00 PM',
                    '6:00 PM - 7:00 PM',
                    '7:00 PM - 8:00 PM'
                ],
                defaultValue: ''
            }]
        }],
        required:false
    }];

    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    var questionsB = [{
        //q1
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Please rate your overall '),
                    makeColored('satisfaction with the service','#862633'),
                    plainText(' you received during your last visit to Jiffy Lube.')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Excellent','Very Good','Average','Fair','Poor'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
    //q2
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('How likely are you to return to Jiffy Lube the next time your vehicle needs an '),
                    makeColored('oil change','#862633'),
                    plainText('?')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Definitely will return','Probably will return','Undecided about returning','Probably will NOT return','Definitely will NOT return'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q3
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('How likely are you to return to Jiffy Lube the next time your vehicle needs '),
                    makeColored('other routine maintenance service','#862633'),
                    plainText('?')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Definitely will return','Probably will return','Undecided about returning','Probably will NOT return','Definitely will NOT return'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q4
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('probably will not return','#862633'),
                    plainText(' to Jiffy Lube the next time you need routine maintenance beyond an oil change?')
                ]),
            questionsPossible: [compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('definitely will return','#862633'),
                    plainText(' to Jiffy Lube the next time you need routine maintenance beyond an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('probably will return','#862633'),
                    plainText(' to Jiffy Lube the next time you need routine maintenance beyond an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('probably will not return','#862633'),
                    plainText(' to Jiffy Lube the next time you need routine maintenance beyond an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you '),
                    makeColored('definitely will not return','#862633'),
                    plainText(' to Jiffy Lube the next time you need routine maintenance beyond an oil change?')
                ]), compileQuestionText(
                [
                    plainText('What about your experience prompts you to say that you are '),
                    makeColored('undecided about returning','#862633'),
                    plainText(' to Jiffy Lube the next time you need routine maintenance beyond an oil change?')
                ])],
            questionType: 'paragraph-group',
            dependsOn: [3,1,1], //question 2, first group, first part
            parts: [{
                defaultValue: ""
            }]
        }],
        required: false
    }, {

        //q5
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Based on your most recent visit at Jiffy Lube, please indicate how well the following statements '),
                    makeColored('describe your experience','#862633'),
                    plainText('.')
                ]),
            questionType: ie9Slider(),
            parts: [{
                label: 'Employees were knowledgeable about my vehicle',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'Employees were friendly and respectful',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'I trusted the service advisor\'s recommendations',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'The service was timely',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'Service was a good value for my money',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'Jiffy Lube employees are well trained to provide services other than oil changes',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }, {
                label: 'Jiffy Lube has the facilities and equipment to provide services other than oil changes',
                sliderPair: ['Strongly Disagree','Strongly Agree'],
                radioOptions: ['Strongly Disagree','Disagree','Neutral','Agree','Strongly Agree'],
                defaultValue: "0"
            }]
        }],
        required: false
    }, {
        //q6
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText(' Many different factors impact where you decide to take your vehicle for service. ' +
                        'For each pair, select the factor that was MOST important when you decided to visit Jiffy Lube for your last service.')
                ]),
            questionType: 'pair-slider-group',
            parts: generatePairs('e')
        }],
        required: false
    }, {
        //q7
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText(' To the best of your knowledge, which of the following services are offered ' +
                        'in addition to oil changes at the Jiffy Lube you visited for your last maintenance service?')
                ]),
            questionType:'picture-select',
            parts: [{
                imagePairs: [
                    [["images/battery_service.png","images/battery_service_checked.png"],
                        ["images/brake_service.png","images/brake_service_checked.png"],
                        ["images/cooling_service.png","images/cooling_service_checked.png"],
                        ["images/fuel_service.png","images/fuel_service_checked.png"]],
                    [["images/tire_rotation.png","images/tire_rotation_checked.png"],
                        ["images/tire_installation.png","images/tire_installation_checked.png"],
                        ["images/transmission_service.png","images/transmission_service_checked.png"],
                        ["images/windshield_service.png","images/windshield_service_checked.png"]],
                    [["images/none.png","images/none_checked.png"]]
                ],
                serviceNames: [
                    'battery maintenance/replacement',
                    'brake services',
                    'cooling system services',
                    'fuel system cleaning services',
                    'tire rotation',
                    'tire installation',
                    'transmission services',
                    'wiper blade replacement',
                    'none of these'
                ],
                defaultValue: ""
            }]
        }],
        required: true
    }, {

        //q8
        groups: [{
            questionText:compileQuestionText(
                [
                    plainText('Would you tell us '),
                    makeColored('about yourself','#862633'),
                    plainText('?')
                ]),
            questionType: 'personal-information-group',
            parts: [{
                label: 'What is your gender?',
                radioOptions: [
                    'Male',
                    'Female',
                    'Other/Prefer Not to Answer'
                ],
                defaultValue: ''
            }, {
                label: 'What is your age group?',
                label2: 'What is your age?',
                //radioOptions: [
                //    '16-35',
                //    '36-55',
                //    '56-75',
                //    '76+',
                //    'Prefer not to answer'
                //],
                radioOptions: [
                    '16-25',
                    '26-35',
                    '36-45',
                    '46-55',
                    '56-65',
                    '66+',
                    'Prefer not to answer'
                ],
                defaultValue: ''
            }]
        }],
        required: false
    }, {

        //q9
        groups: [getQ9()],
        required: !hasToken()
    }, {

        //q10
        groups: [{
            questionText:compileQuestionText(
                [
                    makeColored('(OPTIONAL)','#862633'),
                    plainText(' To discuss any compliments or concerns, ' +
                        'please provide your preferred method of contact (phone or email), and the best time to discuss.')

                ]),
            questionType: 'contact-group',
            parts: [{
                label: 'Name',
                defaultValue: ''
            }, {
                label: 'E-mail',
                defaultValue: ''
            }, {
                label: 'Phone Number',
                defaultValue: ''
            }, {
                label: 'Best days to contact',
                dropOptions: [
                    'Weekdays',
                    'Weekend',
                    'Any'
                ],
                defaultValue: ''
            }, {
                label: 'Best time to contact',
                dropOptions: [
                    '8:00 AM - 9:00 AM',
                    '9:00 AM - 10:00 AM',
                    '10:00 AM - 11:00 AM',
                    '11:00 AM - 12:00 PM',
                    '12:00 PM - 1:00 PM',
                    '1:00 PM - 2:00 PM',
                    '2:00 PM - 3:00 PM',
                    '3:00 PM - 4:00 PM',
                    '4:00 PM - 5:00 PM',
                    '5:00 PM - 6:00 PM',
                    '6:00 PM - 7:00 PM',
                    '7:00 PM - 8:00 PM'
                ],
                defaultValue: ''
            }]
        }],
        required: false
    }];
//************************************************AS

  //  &&&&&&&&&&&&&&

 var questionsAS = [{
        //q1
        groups: [{
            questionText: compileQuestionText(
                [

                    plainText('Califique la satisfacción que, en general, le brindó  el servicio recibido durante súltima visita a Jiffy Lube.')

                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Excelente','Muy buena','Regular','Razonable','Mala'],
                defaultValue: ""

            }]
        }],
        required: true

    }, {
        //q2
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('La próxima vez que su automóvil  necesite un cambio de aceite, ¿qué tan probable es que regrese a Jiffy Lube?')

                ]),
            questionType: 'radio-question',
            parts: [{
                 radioOptions: ['Definitivamente voy a regresar','Probablemente voy a regresar','Indeciso acerca de regresar','Probablemente no voy a regresar','Definitivamente no voy a regresar'],
                defaultValue: ""
            }]
        }],
        required: true

    }, {

        //q3
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('La próxima vez que su automóvil necesite otro servicio rutinario de mantenimiento, ¿qué tan probable es que regrese a Jiffy Lube? ')

                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Definitivamente voy a regresar','Probablemente voy a regresar','Indeciso acerca de regresar','Probablemente no voy a regresar','Definitivamente no voy a regresar'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q4
        groups: [{
            questionText: compileQuestionText(
                [

                    plainText('¿qué aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('probablemente no voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')

                ]),
            questionsPossible: [compileQuestionText(
                [
                        plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                        makeColored('definitivamente voy a regresar','#862633'),
                        plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                    ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('probablemente voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('probablemente no voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('definitivamente no voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('indeciso acerca de regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ])],
            questionType: 'paragraph-group',
            dependsOn: [2,1,1], //question 2, first group, first part
            parts: [{
                defaultValue: ""
            }]
        }],
        required: false
    }, {

        //q5
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Basándonos  en su última visita a Jiffy Lube, ¿qué tan bien describen su punto de vista las siguientes afirmaciones?')
                ]),
            questionType: ie9Slider(),
            parts: [{
                label: 'Los empleados tenían conocimientos sobre mi vehículo',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'Los empleados eran amables y respetuosos',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'Me parecieron confiables las recomendaciones del asesor de servicio',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'El servicio fue puntual',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'El servicio ofreció  una buena relación calidad y precio',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'Los empleados de Jiffy Lube están  debidamente capacitados para ofrecer otros servicios, además del cambio de aceite',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }]
        }],
        required:false
    }, {
        //q6
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Diversos factores influyen al decidir a dónde llevar su vehículo para servicio. ' +
                        'Compare la importancia de cada par de factores a continuación. ' +
                        'Acerque el deslizador al factor que más  influyó  en su decisión de visitar Jiffy Lube para su último servicio.'),
                    // makeColored(' (note: translation still mentions sliders instead of buttons','#ff0000')
                ]),
            questionType: 'pair-slider-group',
            parts: generatePairs('s')
        }],
        required:false

    }, {//q7
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Hasta donde usted sabe, además  del cambio de aceite ¿cuál  de los servicios siguientes ' +
                        'ofrece el centro Jiffy Lube que usted visita para el último servicio de su vehículo?')
                ]),
            questionType:'picture-select',
            parts: [{
                imagePairs: [
                    [["images/battery_service-s.png","images/battery_service_checked-s.png"],
                        ["images/cooling_service-s.png","images/cooling_service_checked-s.png"],
                        ["images/fuel_service-s.png","images/fuel_service_checked-s.png"],
                        ["images/tire_rotation-s.png","images/tire_rotation_checked-s.png"]],
                    [["images/transmission_service-s.png","images/transmission_service_checked-s.png"],
                        ["images/windshield_service-s.png","images/windshield_service_checked-s.png"],
                        ["images/none-s.png","images/none_checked-s.png"]]
                ],
                serviceNames: [
                    'battery maintenance/replacement',
                    'cooling system services',
                    'fuel system cleaning services',
                    'tire rotation',
                    'transmission services',
                    'wiper blade replacement',
                    'none of these'
                ],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q8
        groups: [{
            questionText:compileQuestionText(
                [
                    plainText('¿Puede  hablarnos de usted?')
                ]),
            questionType: 'personal-information-group',
            parts: [{
                label: '¿Cuál es su sexo?',
                radioOptions: [
                    'Masculino',
                    'Femenino',
                    'Prefiero no responder'
                ],
                defaultValue: ''
            }, {
                label: '¿A qué grupo de edad pertenece?',
                radioOptions: [
                    '16-25',
                    '26-35',
                    '36-45',
                    '46-55',
                    '56-65',
                    '66+',
                    'Prefiero no responder'
                ],
                defaultValue: ''
            }, {
                label: 'In what state did you have your vehicle last serviced at Jiffy Lube? (needs translation)',
                defaultValue: ''
            }]
        }],
        required:false
    }, {
        //q9
        groups: [getQ9Spanish()],
        required: !hasToken()
    },
        /*  groups: [{
         questionText: compileQuestionText(
         [
         plainText('Por favor incluya alg�n comentario adicional que pudiera servir para que Jiffy Lube contin�e cont�ndolo entre sus clientes.')
         ]),
         questionType: 'paragraph-group',
         parts: [{
         defaultValue: ""
         }]
         }],
         required:false
         },*/

        {
            //q10
            groups: [{
                questionText:compileQuestionText(
                    [
                        plainText('Si desea que nos comuniquemos con usted para hablar acerca de lo bueno o malo de su ' +
                            'experiencia con nosotros, por favor proporcione su nombre, el método  de contacto que usted prefiere ' +
                            '(teléfono  o correo electrónico) y el mejor horario.')
                    ]),
                questionType: 'contact-group',
                parts: [{
                    label: 'Nombre',
                    defaultValue: ''
                }, {
                    label: 'Correo electrónico',
                    defaultValue: ''
                }, {
                    label: 'Número de teléfono',
                    defaultValue: ''
                }, {
                    label: 'Mejores días para comunicarnos',
                    dropOptions: [
                        'En la semana',
                        'Fin de semana',
                        'Cualquiera'
                    ],
                    defaultValue: ''
                }, {
                    label: 'Mejor horario para comunicarnost',
                    dropOptions: [
                        '8:00 AM - 9:00 AM',
                        '9:00 AM - 10:00 AM',
                        '10:00 AM - 11:00 AM',
                        '11:00 AM - 12:00 PM',
                        '12:00 PM - 1:00 PM',
                        '1:00 PM - 2:00 PM',
                        '2:00 PM - 3:00 PM',
                        '3:00 PM - 4:00 PM',
                        '4:00 PM - 5:00 PM',
                        '5:00 PM - 6:00 PM',
                        '6:00 PM - 7:00 PM',
                        '7:00 PM - 8:00 PM'
                    ],
                    defaultValue: ''

                }]
            }],
            required: false
        }];

   // &&&&&&&&&&&
//***********************************************BSpanish
    var questionsBS = [{
        //q1
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Califique la satisfacción que, en general, le brindó el servicio recibido durante su última visita a Jiffy Lube.')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Excelente','Muy buena','Regular','Razonable','Mala'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q2
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('La próximaa vez que su automóvil necesite un cambio de aceite, ¿qué tan probable es que regrese a Jiffy Lube?')
                ]),
            questionType: 'radio-question',
            parts: [{
                radioOptions: ['Definitivamente voy a regresar','Probablemente voy a regresar','Indeciso acerca de regresar','Probablemente no voy a regresar','Definitivamente no voy a regresar'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q3
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('La próxima vez que su automóvil necesite otro servicio rutinario de mantenimiento, tal como una verificación de los frenos o una reparación, ¿qué  tan probable es que regrese a Jiffy Lube?')
                ]),
            questionType: 'radio-question',
            parts: [{
              radioOptions: ['Definitivamente voy a regresar','Probablemente voy a regresar','Indeciso acerca de regresar','Probablemente no voy a regresar','Definitivamente no voy a regresar'],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
        //q4
        groups: [{
            questionText: compileQuestionText(
                [
                    //plainText('What about your experience prompts you to say that you '),
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('probably will not return','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                   // plainText(' to Jiffy Lube the next time you need an oil change?')
                ]),
            questionsPossible: [compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('definitivamente voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('probablemente voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('probablemente no voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('definitivamente no voy a regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ]), compileQuestionText(
                [
                    plainText('¿qué  aspecto del servicios recibido hoy lo lleva a decir '),
                    makeColored('indeciso acerca de regresar','#862633'),
                    plainText(' a Jiffy Lube la próxima vez que necesite un servicio rutinario de mantenimiento, no sólo un cambio de aceite?')
                ])],
            questionType: 'paragraph-group',
            dependsOn: [3,1,1], //question 3, first group, first part
            parts: [{
                defaultValue: ""
            }]
        }],
        required: false
    }, {
        //q5
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Basándonos en su última  visita a Jiffy Lube, ¿qué  tan bien describen su punto de vista las siguientes afirmaciones?')
                ]),
            questionType: ie9Slider(),
            parts: [{
                label: 'Los empleados tenían conocimientos sobre mi vehículo',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'Los empleados eran amables y respetuosos',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'Me parecieron confiables las recomendaciones del asesor de servicio',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'El servicio fue puntual',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'El servicio ofreció una buena relación calidad y precio',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'Los empleados de Jiffy Lube están debidamente capacitados para ofrecer otros servicios, además del cambio de aceite',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }, {
                label: 'Jiffy Lube tiene las instalaciones y el equipo necesario para ofrecer otros servicios, además del cambio de aceite',
                sliderPair: ['Totalmente en desacuerdo','Totalmente de acuerdo'],
                radioOptions: ['Fuertemente En desacuerdo','En desacuerdo','Neutral','De acuerdo','Fuertemente De acuerdo'],
                defaultValue: "0"
            }]
        }],
        required: false
    }, {
        //q6
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Diversos factores influyen al decidir a dónde llevar su vehículo para servicio. ' +
                        'Compare la importancia de cada par de factores a continuación. ' +
                        'Acerque el deslizador al factor que más nfluyó en su decisión de visitar Jiffy Lube para su último servicio.'),
                  //  makeColored(' (note: translation still mentions sliders instead of buttons','#ff0000')
                ]),
            questionType: 'pair-slider-group',
            parts: generatePairs('s')
        }],
        required: false
    }, {
    //q7
        groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Hasta donde usted sabe, además del cambio de aceite ¿cuál de los servicios siguientes ' +
                        'ofrece el centro Jiffy Lube que usted visitó para el último  servicio de su vehículo?')
                ]),
            questionType:'picture-select',
            parts: [{
                imagePairs: [
                    [["images/battery_service-s.png","images/battery_service_checked-s.png"],
                        ["images/brake_service-s.png","images/brake_service_checked-s.png"],
                        ["images/cooling_service-s.png","images/cooling_service_checked-s.png"],
                        ["images/fuel_service-s.png","images/fuel_service_checked-s.png"]],
                    [["images/tire_rotation-s.png","images/tire_rotation_checked-s.png"],
                        ["images/tire_installation-s.png","images/tire_installation_checked-s.png"],
                        ["images/transmission_service-s.png","images/transmission_service_checked-s.png"],
                        ["images/windshield_service-s.png","images/windshield_service_checked-s.png"]],
                    [["images/none-s.png","images/none_checked-s.png"]]
                ],
                serviceNames: [
                    'battery maintenance/replacement',
                    'brake services',
                    'cooling system services',
                    'fuel system cleaning services',
                    'tire rotation',
                    'tire installation',
                    'transmission services',
                    'wiper blade replacement',
                    'none of these'
                ],
                defaultValue: ""
            }]
        }],
        required: true
    }, {
    //q8
        groups: [{
            questionText:compileQuestionText(
                [
                    plainText('¿Puede hablarnos de usted?')
                ]),
            questionType: 'personal-information-group',
            parts: [{
                label: '¿Cuál es su sexo?',
                radioOptions: [
                    'Masculino',
                    'Femenino',
                    'Prefiero no responder'
                ],
                defaultValue: ''
            }, {
                label: '¿A qué  grupo de edad pertenece?',
                radioOptions: [
                    '16-25',
                    '26-35',
                    '36-45',
                    '46-55',
                    '56-65',
                    '66+',
                    'Prefiero no responder'
                ],
                defaultValue: ''
            }, {
                label: 'In what state did you have your vehicle last serviced at Jiffy Lube? (needs translation)',
                defaultValue: ''
            }]
        }],
        required: false,
    }, {



            //q9
            groups: [getQ9Spanish()],
            required: !hasToken()

    },
        //q9
       /* groups: [{
            questionText: compileQuestionText(
                [
                    plainText('Por favor incluya alg�n comentario adicional que pudiera servir para que Jiffy Lube contin�e cont�ndolo entre sus clientes.')
                ]),
            questionType: 'paragraph-group',
            parts: [{
                defaultValue: ""
            }]
        }]
    },*/


        {
        //q10
        groups: [{
            questionText:compileQuestionText(
                [
                    plainText('Si desea que nos comuniquemos con usted para hablar acerca de lo bueno o malo de su ' +
                        'experiencia con nosotros, por favor proporcione su nombre, el método  de contacto que usted prefiere ' +
                        '(teléfono  o correo electrónico) y el mejor horario.')
                ]),
            questionType: 'contact-group',
            parts: [{
                label: 'Nombre',
                defaultValue: ''
            }, {
                label: 'Correo electrónico',
                defaultValue: ''
            }, {
                label: 'Número de teléfono',
                defaultValue: ''
            }, {
                label: 'Mejores días para comunicarnos',
                dropOptions: [
                    'En la semana',
                    'Fin de semana',
                    'Cualquiera'
                ],
                defaultValue: ''
            }, {
                label: 'Mejor horario para comunicarnost',
                dropOptions: [
                    '8:00 AM - 9:00 AM',
                    '9:00 AM - 10:00 AM',
                    '10:00 AM - 11:00 AM',
                    '11:00 AM - 12:00 PM',
                    '12:00 PM - 1:00 PM',
                    '1:00 PM - 2:00 PM',
                    '2:00 PM - 3:00 PM',
                    '3:00 PM - 4:00 PM',
                    '4:00 PM - 5:00 PM',
                    '5:00 PM - 6:00 PM',
                    '6:00 PM - 7:00 PM',
                    '7:00 PM - 8:00 PM'
                ],
                defaultValue: ''

            }]
        }],
            required: false
    }];

    //savedResponses exists outside PageController so that the jQuery AJAX call can retrieve the information it needs.
    var savedResponses = [1,2,3,4];

    //adds calendar UI to optional Q9 date question
    $(function() {
        $( "#datepicker" ).datepicker();
    });

    //the ajax call. posts savedResponses to the server
    $(document).ready(function(){
        $("#submit").click(function(){

            var tokenArray = savedResponses.token.split('-');
            var showNoCoupon = false;
            //check to see if token contains the extra -0 or -1
            if (tokenArray.length>5) {
                if (tokenArray[4] == '1')
                    showNoCoupon = true;
                savedResponses.token = tokenArray[0] + '-' + tokenArray[1] + '-' +tokenArray[2] + '-' +
                    tokenArray[3] + '-' + tokenArray[5];
            }
            //alert(savedResponses.token);

            $.ajax(
                {
                    type: "POST",
                    url: "http://ec2-54-175-145-19.compute-1.amazonaws.com:8080/jli-api/postgre/postdata",//http://ec2-54-175-145-19.compute-1.amazonaws.com:8080/jli-api/postgre/postdata
                    data: JSON.stringify(savedResponses),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data,textStatus,jqXHR) {
                        //alert(data);

                        if(showNoCoupon)
                            window.location.assign(' http://survey.jiffylube.com/thankyou.aspx?token='+parse('token',''));
                        else
                            window.location.assign('http://survey.jiffylube.com/?token='+parse('token',''));
                    },
                    error: function (jqXHR,textStatus, errorThrown ) {
                        //alert(errorThrown);
                    }
                }
            );
        });
    });


})();
